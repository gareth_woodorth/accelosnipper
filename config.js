module.exports = {
	templateEngine: 'ejs',
	autoSaveInterval: 2,
	ide: {
		ATOM: 'atom',
		SUBLIME: 'sublime',
		VS: 'vs'
	},
	options: {
		ATOM: {
			templateFile: 'atom.ejs'
		},
		SUBLIME: {
			templateFile: 'sublime.ejs'
		}
	}
};