const Snippet = require('./util/snippet');
const transformer = require('./util/transformer');
const snippets = createSnippets();


console.log(transformer.transform(snippets, 'atom'));
console.log(transformer.transform(snippets, 'vs'));
console.log(transformer.transform(snippets, 'sublime'));

function createSnippets() {
	const snippets = [];
	let snippet = new Snippet();

	snippet.shortcut = 'html';
	snippet.source = 'html';
	snippet.title = 'html';
	snippet.body = `<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>$1</title>
</head>
<body>
<h1>$2</h1>
<p>
  $3
</p>
</body>
</html>`;
	snippet.desc = 'Dumper';

	snippets.push(snippet);

	snippet = new Snippet();

	snippet.shortcut = 'log';
	snippet.source = 'js';
	snippet.title = 'console.log';
	snippet.body = 'console.log(${1:"crash"});$2';

	snippets.push(snippet);

	snippet = new Snippet();

	snippet.shortcut = 'dmp';
	snippet.source = 'js';
	snippet.title = 'Warn to dumper';
	snippet.body = 'use Data::Dumper;\nwarn Dumper $1;';
	snippet.desc = 'Dumper';

	snippets.push(snippet);
	return snippets;
}