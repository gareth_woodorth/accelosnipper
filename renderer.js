const electron = require('electron');
const {ipcRenderer} = electron;
const $ = window.$ = window.jQuery = require('jquery');
const config = require('./config');
require('jquery-ui-bundle');

const defaultSnippets = [{
	source: 'perl',
	title: 'dumper',
	shortcut: 'dmp',
	desc: 'use for logging',
	body: 'use Data::Dumper; warn Dumper "something";',
}, {
	source: 'js',
	title: 'dumper2',
	shortcut: 'dmp',
	desc: 'use for logging',
	body: 'wowser;',
}, {
	source: '',
	title: 'dumper2',
	shortcut: 'dmp',
	desc: 'use for logging',
	body: 'none',
}];

let selectedIndex = 0;
let snippets = [];

// Events will go here for communication into main.js and back down to the front end 

$(function () {
	const snippetSelect = $('.snippetSelect');

	snippetSelect.on('click', '.snippet-delete', function (event) {
		event.preventDefault();
		let snippetElements = $('.snippet');
		let snippetElement = $(this).closest('.snippet');
		let index = snippetElements.index(snippetElement);

		snippetElement.remove();
		snippets.splice(index, 1);
		//run out of snippets
		if (snippets.length <= 0) {
			selectedIndex = -1;
		} else {
			selectedIndex = (index - 1 < 0) ? 0 : index - 1;
			loadSnippet(selectedIndex);
		}
		$('.snippetsContainer').sortable('refresh');
	});

	snippetSelect.on('click', '.snippet', function (event) {
		let index = $('.snippet').index(this);
		loadSnippet(index);
	});

	$('#source').on('change', function (event) {
		snippets[selectedIndex].source = event.target.value;
		onSourceChanged(selectedIndex, event.target);
	});


	$('.snippet-input').on('keydown keyup', function (event) {
		if (selectedIndex > -1) {
			const snippet = snippets[selectedIndex];
			snippet[event.target.name] = event.target.value;
			$('.snippet-title').eq(selectedIndex).text(snippet.title);
		}
	});

	$('.accordionHeader').on('click', function () {
		if ($(this).next().is(':visible')) {
			$(this).next().slideUp(300);
			$(this).children("img").attr('src', 'logos/plus.png');
		} else {
			$(this).next().slideDown(300);
			$(this).children("img").attr('src', 'logos/minus.png');
		}
	});

	$('#addButton').on('click', function (event) {
		const newSnippet = {
			source: '',
			title: 'new snippet',
			shortcut: '',
			desc: '',
			body: '',
		};
		snippets.push(newSnippet);
		createSnippet(newSnippet);
		loadSnippet(snippets.length - 1);
		$(".snippetsContainer").sortable('refresh');
	});

	setInterval(() => {
		ipcRenderer.send('autoSave', snippets);
	}, config.autoSaveInterval * 1000);


});


function loadSnippet(index) {
	if (indexWithinRange(index)) {
		if (snippets.length > 0) {
			let snip = snippets[index];

			$('#source').val(snip.source);
			$('#title').val(snip.title);
			$('#shortcut').val(snip.shortcut);
			$('#desc').val(snip.desc);
			$('#body').val(snip.body);

			selectSnippet(index);
		}
	}
}

function indexWithinRange(index) {
	return index >= 0 && index < snippets.length;
}

function selectSnippet(index) {
	const snippet = $('.snippet');
	snippet.removeClass('snippet--selected');
	selectedIndex = index;
	snippet.eq(index).addClass('snippet--selected');
}

function sortSnippet(snippet) {
	let targetElement;
	if (snippet.source) {
		targetElement = $(`#${snippet.source}Snippets`);
	} else {
		targetElement = $('#snippets');
	}
	console.log('move to ', targetElement.attr('id'));

	return targetElement;
}

function onSourceChanged(index) {
	const snippet = snippets[index];
	let targetElement = sortSnippet(snippet);
	let snippetElement = $('.snippet--selected');
	let snippetContainers = $('.snippetsContainer');
	let elementIndex = 0;
	for (let snippetContainer of [...snippetContainers]) {
		elementIndex += $(snippetContainer).children().length;
		if (snippetContainer.id == targetElement.get(0).id) {
			break;
		}
	}
	targetElement.append(snippetElement);
	moveElement(index, elementIndex, snippet);
}

function moveElement(oldIndex, newIndex, snippet) {
	if (oldIndex == selectedIndex) {
		selectedIndex = newIndex;
	}
	snippets.splice(oldIndex, 1);
	snippets.splice(newIndex, 0, snippet);
}

function createSnippet(snippet) {
	let targetElement = sortSnippet(snippet);
	targetElement.append(`<div class='snippet snippetSelect__option flex space-between'>
<div class="snippet-title">${snippet.title}</div>
<img class="snippet-delete" src="logos/delete.png"/>
</div>`);
}

$('.logoButton').on('click', function (event) {
	if (selectedIndex > -1) {
		let snippet = snippets[selectedIndex];
		const IDE = $(this).data('type');
		const isConvertAll = $("#convertAll").is(':checked');
		const convertedSnippets = isConvertAll ? snippets : [snippet];
		ipcRenderer.send('convert', {
			IDE: IDE,
			snippets: convertedSnippets
		});
	}
});


ipcRenderer.on('result', (event, specialisedSnippet) => {
	document.querySelector('#result').innerHTML = specialisedSnippet;
});

ipcRenderer.on('loadedSnippets', (event, loadedSnippets) => {
	if (loadedSnippets && loadedSnippets.length) {
		snippets = loadedSnippets;
		loadSnippet(selectedIndex);
	} else {
		snippets = defaultSnippets;
	}

	$.each(snippets, function (index, snippet) {
		//append to #snippets
		createSnippet(snippet);
	});

	$(".snippetsContainer").sortable({
		connectWith: '.snippetsContainer',
		start: function (event, ui) {
			const oldIndex = $('.snippet').index(ui.item[0]);
			$(this).attr('data-previndex', oldIndex);
		},
		dropOnEmpty: true,
		cursor: 'crosshair',
		update: function (event, ui) {
			const oldIndex = $(this).attr('data-previndex');
			if (oldIndex !== undefined) {
				const parent = ui.item.parent();
				const newSource = parent.data('source');
				const newIndex = $('.snippet').index(ui.item[0]);
				if (oldIndex == selectedIndex) {
					$("#source").val(newSource);
				}
				$(this).removeAttr('data-previndex');
				const snippet = snippets[oldIndex];
				//update source
				snippet.source = newSource;
				moveElement(oldIndex, newIndex, snippet);
			}
		},
	});
	loadSnippet(0);
});
