const Adapter = require('./adapter');

class SublimeAdapter extends Adapter {
	constructor(config, templateProcessor) {
		super(config);
		this.templateProcessor = templateProcessor;
	}

	transform(snippets) {
		const result = [];
		for (let snippet of snippets) {
			result.push(this.templateProcessor.process(this.template, {
				body: snippet.body,
				shortcut: snippet.shortcut,
				source: `.source.${snippet.source}`
			}));
		}
		return result.join("\n\n\n\n==============================\n\n\n\n");
	}
}

module.exports = SublimeAdapter;