const Adapter = require('./adapter');

class AtomAdapter extends Adapter {
	constructor(config, templateProcessor) {
		super(config);
		this.templateProcessor = templateProcessor;
	}

	transform(snippets) {
		let sourceGroup, sources = [];
		sourceGroup = snippets.reduce((sourceGroup, snippet) => {
			let source = sourceGroup[snippet.source];
			if (!source) {
				source = {snippets: [], title: `.source.${snippet.source}`};
				sourceGroup[snippet.source] = source;
			}
			source.snippets.push({
				body: snippet.body,
				shortcut: snippet.shortcut,
				title: snippet.title,
			});
			return sourceGroup;
		}, {});
		Object.keys(sourceGroup).forEach((source) => {
			sources.push(sourceGroup[source]);
		});
		return this.templateProcessor.process(this.template, {
			sources,
		});
	}
}

module.exports = AtomAdapter;