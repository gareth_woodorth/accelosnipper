const Adapter = require('./adapter');

class VsAdapter extends Adapter {
	transform(snippets) {
		let outputSnippets = {};
		for (let snippet of snippets) {
			const body = snippet.body;
			const lines = body.split('\n');
			outputSnippets[snippet.title] = {
				prefix: snippet.prefix,
				body: lines,
				description: snippet.desc,
			};
		}
		return JSON.stringify(outputSnippets, null, 2);
	}
}

module.exports = VsAdapter;