const fs = require('fs');
const path = require("path");

class Adapter {
	constructor(config) {
		if (config.templateFile) {
			this.template = fs.readFileSync(path.join(__dirname, '..', 'templates', config.templateFile)).toString();
		}
	}
}

module.exports = Adapter;
