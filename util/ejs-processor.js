const ejs = require('ejs');

class EjsProcessor {
	constructor(options) {
		this.options = options;
	}

	process(template, data) {
		const templateData = ejs.compile(template, this.options);
		return templateData(data);
	}
}

module.exports = EjsProcessor;