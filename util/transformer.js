const config = require('../config');
const AtomAdapter = require('../adapters/atom-adapter');
const SublimeAdapter = require('../adapters/sublime-adapter');
const VsAdapter = require('../adapters/vs-adapter');
const EjsProcessor = require('./ejs-processor');

class Transformer {
	constructor() {
		this.init();
	}

	init() {
		this.templateProcessor = this._getTemplateProcessor(config.templateEngine);
		this.atomAdapter = new AtomAdapter(config.options.ATOM || {}, this.templateProcessor);
		this.vsAdapter = new VsAdapter(config.options.VS || {});
		this.sublimeAdapter = new SublimeAdapter(config.options.SUBLIME || {}, this.templateProcessor);
	}

	transform(snippets, ide) {
		const adapter = this._getAdapter(ide);
		return adapter.transform(snippets);
	}

	stringifySnippets(snippets) {
		return JSON.stringify(snippets, null, 2);
	}

	stringToObjects(string) {
		return JSON.parse(string);
	}

	_getAdapter(ide) {
		switch (ide) {
			case config.ide.VS:
				return this.vsAdapter;
			case config.ide.SUBLIME:
				return this.sublimeAdapter;
			default:
				return this.atomAdapter;
		}
	}

	_getTemplateProcessor(templateEngine) {
		switch (templateEngine) {
			case 'ejs':
				return new EjsProcessor({});
		}
	}
}

module.exports = new Transformer();
