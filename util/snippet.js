class Snippet {
	set source(source) {
		this._source = source;
	}

	get source() {
		return this._source;
	}

	set title(title) {
		this._title = title;
	}

	get title() {
		return this._title;
	}

	set shortcut(shortcut) {
		this._shortcut = shortcut;
	}

	get shortcut() {
		return this._shortcut;
	}

	set body(body) {
		this._body = body;
	}

	get body() {
		return this._body;
	}

	set desc(description) {
		this._description = description;
	}

	get desc() {
		return this._description;
	}
}

module.exports = Snippet;

